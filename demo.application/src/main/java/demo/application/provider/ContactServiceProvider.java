/*
 * ContactServiceProvider.java
 *
 * Created on Jan 13, 2021, 5:39 PM
 */
package demo.application.provider;

import demo.domain.repository.ContactRepo;
import demo.domain.service.ContactService;
import demo.infra.data.repository.ContactRepoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author Asus
 */
@Configuration
public class ContactServiceProvider
{
    @Autowired
    private ContactRepoImpl repo;
    private ContactService service;

    public ContactServiceProvider()
    {
        System.out.println("ContactServiceProvider() CTOR called.");
        if (repo == null)
            System.out.println("EMANG NULL");

        repo = new ContactRepoImpl();
        service = new ContactService(repo);
    }

    @Bean
    public ContactService getServiceObject()
    {
        return service;
    }
}
