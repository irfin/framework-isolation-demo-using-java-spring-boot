/*
 * ContactCtrl.java
 *
 * Created on Jan 13, 2021, 5:37 PM
 */
package demo.application.http.controller;

import demo.application.provider.ContactServiceProvider;
import demo.domain.entity.Contact;
import demo.domain.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Asus
 */
@RestController
@RequestMapping("/api/contacts")
public class ContactCtrl
{
    @Autowired
    private ContactServiceProvider contactServiceProvider;

    @GetMapping
    public Iterable<Contact> getAll()
    {
        ContactService svc = contactServiceProvider.getServiceObject();
        return svc.getAll();
    }

    @PostMapping
    public Contact createNew(@RequestBody Contact data)
    {
        ContactService svc = contactServiceProvider.getServiceObject();
        svc.create(data);

        return data;
    }
}
