package demo.application;/*
 * Copyright (c) 2020 Sunwell System PT.
 * Seluruh kode sumber ini adalah hak cipta PT. Sunwell System, kecuali disebutkan berbeda.
 */

/*
 * demo.application.DemoApp.java
 *
 * Created On Feb 20, 2020, 13.14
 */
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Aditia Dwi
 * @author Irfin A
 * @author Christian Hardy
 */
@SpringBootApplication(
        scanBasePackages = {"demo.application", "demo.domain", "demo.infra.data"}
)
public class DemoApp implements CommandLineRunner
{
    public static void main(String[] args)
    {
        SpringApplication.run(DemoApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception
    {
    }
}
