/*
 * Mahasiswa.java
 *
 * Created on Jan 13, 2021, 4:21 PM
 */
package demo.domain.entity;

import javax.persistence.*;

/**
 * @author Asus
 */
@Entity
public class Contact
{
    @Id
    @GeneratedValue
    private Long systemid;

    @Column
    private String fullname;

    @Column
    private String address;

    public Long getSystemid()
    {
        return systemid;
    }

    public void setSystemid(Long systemid)
    {
        this.systemid = systemid;
    }

    public String getFullname()
    {
        return fullname;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
