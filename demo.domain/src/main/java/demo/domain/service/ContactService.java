/*
 * ContactService.java
 *
 * Created on Jan 13, 2021, 4:24 PM
 */
package demo.domain.service;

import demo.domain.entity.Contact;
import demo.domain.repository.ContactRepo;

import javax.persistence.EntityManager;

/**
 * @author Asus
 */
public class ContactService
{
    private ContactRepo repo;
    private EntityManager em;

    public ContactService(ContactRepo repoObj)
    {
        repo = repoObj;
        em = repo.getEntityManager();
    }

    public void create(Contact c)
    {
        try {
            /// Ketikga operasi DB di bawah tercakup dalam sebuah Unit of Work.
            repo.create(c);

            em.getTransaction().commit();
        }
        catch (Exception x) {
            em.getTransaction().rollback();
        }
    }

    public Iterable<Contact> getAll()
    {
        return repo.findAll();
    }
}
