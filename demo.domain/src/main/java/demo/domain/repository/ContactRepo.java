/*
 * ContactRepo.java
 *
 * Created on Jan 13, 2021, 4:26 PM
 */
package demo.domain.repository;

import demo.domain.entity.Contact;

import javax.persistence.EntityManager;

/**
 * @author Asus
 */
public interface ContactRepo
{
    public Contact create(Contact c);
    public boolean update(Long id, Contact c);
    public Iterable<Contact> findAll();
    public Contact findById(Long id);

    public EntityManager getEntityManager();
}
