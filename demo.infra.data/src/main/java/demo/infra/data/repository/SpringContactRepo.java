/*
 * SpringContactRepo.java
 *
 * Created on Jan 13, 2021, 5:14 PM
 */
package demo.infra.data.repository;

import demo.domain.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Asus
 */
@Repository
public interface SpringContactRepo extends JpaRepository<Contact, Long>
{
}
