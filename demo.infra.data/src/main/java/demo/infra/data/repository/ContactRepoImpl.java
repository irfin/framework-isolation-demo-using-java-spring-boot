/*
 * demo.infra.data.repository.ContactRepoImpl.java
 *
 * Created on Jan 13, 2021, 5:12 PM
 */
package demo.infra.data.repository;

import demo.domain.entity.Contact;
import demo.domain.repository.ContactRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

/**
 * @author Asus
 */
@Component
public class ContactRepoImpl implements ContactRepo
{
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private SpringContactRepo springRepo;

    public ContactRepoImpl()
    {
        System.out.println("ContactRepoImpl() CTOR called");
        if (em != null)
            System.out.println("EM NYA GAK NULL");
    }

    @Override
    public Contact create(Contact contact)
    {
        springRepo.save(contact);
        return contact;
    }

    @Override
    public boolean update(Long id, Contact contact)
    {
        springRepo.save(contact);
        return true;
    }

    @Override
    public Iterable<Contact> findAll()
    {
        return springRepo.findAll();
    }

    @Override
    public Contact findById(Long id)
    {
        return springRepo.findById(id).get();
    }

    @Override
    public EntityManager getEntityManager()
    {
        return em;
    }
}
